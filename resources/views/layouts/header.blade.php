<nav class="p-5 navbar navbar-expand-lg navbar-light bg-transparent">
  <a class="navbar-brand f-s-50 text-light" href="/"><i class="fas fa-home fa-2x"></i></a>
  <span class="font-robotolight text-green px-5 border-bottom border-green">
      R$200,00
  </span>
  <button class="navbar-toggler outline-none border-0 text-light" type="button" data-toggle="collapse" data-target="#navbar" aria-controls="navbar" aria-expanded="false">
    <span class=""><i class="fas fa-bars fa-5x"></i></span>
  </button>

  <div class="collapse navbar-collapse row w-100 mx-0 bg-transparent" id="navbar">
    {{-- <ul class="navbar-nav px-0 col-12 font-robotolight">
        <div class="row w-100 mx-0 justify-content-between mt-4">
            <li class="nav-item col-3 themeBtn h-100 text-center align-middle">
                <a class="nav-link w-100 text-white py-4 text-decoration-none f-s-45" href="#">geral</a>
            </li>
            <li class="nav-item col-3 themeBtn h-100 text-center align-middle">
                <a class="nav-link w-100 text-white py-4 text-decoration-none f-s-45" href="#">rendas</a>
            </li>
        </div>
    </ul> --}}
    {{-- <form class="form-inline my-2 col-12 px-0">
      <input class="form-control mr-sm-2" type="search" placeholder="Buscar" aria-label="Buscar">
      <button class="btn btn-outline-success my-2 my-sm-0 f-s-30" type="submit">Search</button>
    </form> --}}
  </div>
</nav>
