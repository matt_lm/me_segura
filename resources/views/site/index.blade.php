@extends('layouts.main') @section('content') @section('title', 'Home')
    <div class="m-5">
        <span class="f-s-80 text-white font-robotobold">Atalhos</span>
        <br><br>
        <div class="row mt-5">
            <div class="col-sm-12 py">
                {{-- CAPSULE START --}}
                <a href="#" class="text-decoration-none text-green">
                    <div class="w-100 text-green capsule radius-">
                        <span class="capsule-top-ico">
                            <i class="fas fa-shopping-cart fa-2x"></i>
                        </span>
                        <div class="row justify-content-end">
                            <div class="col-sm-10 pl-5">
                                <span class="f-s-70 text-white font-robotolight">Adicionar compra</span>
                            </div>
                        </div>
                            <span class="capsule-bottom-ico">
                                <i class="fas fa-plus-circle fa-lg"></i>
                            </span>
                    </div>
                </a>
                {{-- CAPSULE END --}}
                {{-- CAPSULE START --}}
                <a href="#" class="text-decoration-none text-green">
                    <div class="w-100 text-green capsule radius-10">
                        <span class="capsule-top-ico">
                            <i class="fas fa-file-alt fa-2x"></i>
                        </span>
                        <div class="row justify-content-end">
                            <div class="col-sm-10 pl-5">
                                <span class="f-s-70 text-white font-robotolight">Lista de compras</span>
                            </div>
                        </div>
                            <span class="capsule-bottom-ico">
                                <i class="fas fa-arrow-right fa-lg"></i>
                            </span>
                    </div>
                </a>
                {{-- CAPSULE END --}}
            </div>
        </div>
    </div>
@endsection
@section('script')
@endsection
